# Simple Note #

Note application on Angular2 and Node.js

### How to start ? ###

* clone repo
* npm install in root directory will install all dependences for the client
* navigate to 'server' folder and install all packages for the server. do npm install
* start the client execute npm start in root dir
* start the server - navigate to  'server' folder and type npm start in console. I used nodemon to run server.js file

You need to have installed node and npm on your machine. I used node 6.10

