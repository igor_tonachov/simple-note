var express = require('express');
var router = express.Router();
const uuidV4 = require('uuid/v4');
var _ = require('lodash');
var notes = require('../data/notes.json');
var fs = require('fs');


router.get('/', function(req, res){
  res.json({notes: notes});
});

router.get('/:id', function(req, res){
  note = notes.find((item) =>{
    return item.id === req.params.id;
  })
  res.json({note: note});
});

router.post('/', function(req, res){
    note = req.body;
    note.id = uuidV4();
    notes.unshift(req.body);
    fs.writeFile('./data/notes.json', JSON.stringify(notes, null, 2 ), 'utf8', function(err) {
    if (err) {
      console.log(err);
    }
    res.json({note: note});
  });
});

router.delete('/:id', function(req, res){
  note = notes.splice(req.params.id, 1);
  fs.writeFile('./data/notes.json', JSON.stringify(notes, null, 2 ), 'utf8', function(err) {
    if (err) {
      console.log(err);
    }
  });
  res.json(note);
});


router.put('/:id', function(req, res){
  let id = req.params.id;
  note = req.body;
  for(var i = 0; i < notes.length; i++){
    if(notes[i].id === id){
      notes[i] = note;
    }
  }
  fs.writeFile('./data/notes.json', JSON.stringify(notes, null, 2 ), 'utf8', function(err) {
    if (err) {
      console.log(err);
    }
  });
  res.json(note);
});

 module.exports = router;
