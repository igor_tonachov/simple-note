var express = require('express');
var app = express();
var cors = require('cors');
var route = express.Router();
var bodyParser = require('body-parser');
var uuid = require('uuid');

app.set('port', process.env.PORT || 4000);
app.use(cors({
    origin: ['http://localhost:3000'],
    credentials: true
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/notes', require('./routes/notes'));
var server = app.listen(app.get('port'), function(){
  console.log("Listen  on port: " + app.get('port'));
})
