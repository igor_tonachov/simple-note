"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
// import { NoteComponent } from './note/note.component';
var index_1 = require("./note/index");
var appRoutes = [
    { path: '', component: index_1.NoteComponent },
    { path: 'new', component: index_1.CreateNoteComponent },
    { path: 'edit/:id', component: index_1.EditNoteComponent },
    { path: '**', redirectTo: '' }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map