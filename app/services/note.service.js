"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
var NoteService = (function () {
    function NoteService(http) {
        this.http = http;
        this.apiUrl = 'http://localhost:4000';
        this.categories = ['Technology', 'Traveling', 'Games', 'Sport', 'Design', 'Arhitecture'];
        this.colors = ["#f1c40f", "#1abc9c", "#3498db", "#e74c3c", "#9b59b6", "#e67e22", "#ffb6c1"];
        this.notes = [];
        this.note_settings = {
            categories: this.categories,
            colors: this.colors
        };
    }
    NoteService.prototype.getNoteSettings = function () {
        return this.note_settings;
    };
    NoteService.prototype.getMyNotes = function () {
        return this.notes;
    };
    NoteService.prototype.getNotes = function () {
        var _this = this;
        return this.http.get(this.apiUrl + '/notes')
            .map(function (res) {
            _this.notes = res.json().notes;
            return res.json();
        });
    };
    NoteService.prototype.getById = function (id) {
        return this.http.get(this.apiUrl + '/notes/' + id)
            .map(function (res) { return res.json(); });
    };
    NoteService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(this.apiUrl + '/notes/' + id)
            .map(function (res) {
            var note_id = res.json()[0].id;
            _this.notes = _this.notes.filter(function (item) {
                return item.id !== note_id;
            });
            res.json();
        });
    };
    NoteService.prototype.create = function (note) {
        var _this = this;
        return this.http.post(this.apiUrl + '/notes', note)
            .map(function (res) {
            _this.notes.unshift(res.json().note);
            return res.json();
        });
    };
    NoteService.prototype.update = function (note) {
        var _this = this;
        return this.http.put(this.apiUrl + '/notes/' + note.id, note)
            .map(function (res) {
            var note_id = res.json().id;
            for (var i = 0; i < _this.notes.length; i++) {
                if (_this.notes[i].id === note_id) {
                    _this.notes[i] = res.json();
                }
            }
            return res.json();
        });
    };
    return NoteService;
}());
NoteService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], NoteService);
exports.NoteService = NoteService;
//# sourceMappingURL=note.service.js.map