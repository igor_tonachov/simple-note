import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Note } from '../models/index';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()

export class NoteService {
  public readonly apiUrl = 'http://localhost:4000';

  categories:string[] = ['Technology','Traveling','Games','Sport','Design','Arhitecture'];
  colors:string[] = ["#f1c40f", "#1abc9c", "#3498db", "#e74c3c", "#9b59b6", "#e67e22", "#ffb6c1"];
  notes:Note[] = [];

  note_settings:any = {
    categories: this.categories,
    colors: this.colors
  }

  constructor(private http: Http){}

  getNoteSettings(){
    return this.note_settings;
  }


  getMyNotes(){
    return this.notes;
  }
  getNotes() {
    return this.http.get(this.apiUrl + '/notes')
       .map((res:any) =>{
         this.notes = res.json().notes;
         return res.json()
       });
  }
  getById(id:string) {
    return this.http.get(this.apiUrl + '/notes/' + id)
       .map((res:any) => res.json());
  }

  delete(id:string){
    return this.http.delete(this.apiUrl + '/notes/' + id)
    .map((res:any) => {
      let note_id:string = res.json()[0].id;
      this.notes = this.notes.filter(item => {
        return item.id !== note_id;
      })
      res.json()
    });
  }

  create(note:Note){
    return this.http.post(this.apiUrl + '/notes', note)
    .map((res:any) => {
      this.notes.unshift(res.json().note);
      return res.json()
    });
  }

  update(note:Note){
    return this.http.put(this.apiUrl + '/notes/' + note.id, note)
    .map((res:any) => {
      let note_id:string = res.json().id;
      for(var i = 0; i < this.notes.length; i ++ ){
        if(this.notes[i].id === note_id){
          this.notes[i] = res.json();
        }
      }

      return res.json()
    });
  }
}
