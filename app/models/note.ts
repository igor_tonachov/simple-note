export class Note {
  id?: string;
  title: string;
  description: string;
  color: string;
  categories: string;
  tags?: string[];
}
