﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { NoteService } from './services/index';

import { CreateNoteComponent, NoteColorComponent, NoteComponent, EditNoteComponent } from './note/index';



@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        NoteComponent,
        CreateNoteComponent,
        EditNoteComponent,
        NoteColorComponent
    ],
    providers: [
        NoteService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
