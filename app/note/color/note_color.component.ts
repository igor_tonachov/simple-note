import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { NoteService } from '../../services/index';

@Component ({
  moduleId: module.id,
  selector: 'note-color',
  templateUrl: 'color.template.html',
  styles: [`
          .like-label {
            text-transform: uppercase;
            font-size: 10px;
            display:inline-block;
            margin-bottom:5px;
          }
          .color-wrapper{
            margin-bottom: 15px;
          }
          .color{
            display: inline-block;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            margin-right: 10px;
            cursor: pointer;
            box-shadow: 1px 1px 2px rgba(0,0,0,.3);
          }
          .active {
            background: #444;
            width: 40px;
            height: 40px;
            border:solid #fff 5px;
            box-shadow: 0 0 2px 3px rgba(43, 162, 224, .6);
          }
  `]
})

export class NoteColorComponent implements OnInit{
  @Input() selectedColor:string
  @Output() newValue: EventEmitter<any> = new EventEmitter<any>();

  constructor(private noteService :NoteService) {}
  colors: string[] = [];

  getColor(e:Event, color:string){
    this.selectedColor = color;
    this.newValue.emit(color);
  }

  ngOnInit() {
    let settings = this.noteService.getNoteSettings();
    this.colors = settings.colors;
  }
}
