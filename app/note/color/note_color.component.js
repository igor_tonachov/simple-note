"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../../services/index");
var NoteColorComponent = (function () {
    function NoteColorComponent(noteService) {
        this.noteService = noteService;
        this.newValue = new core_1.EventEmitter();
        this.colors = [];
    }
    NoteColorComponent.prototype.getColor = function (e, color) {
        this.selectedColor = color;
        this.newValue.emit(color);
    };
    NoteColorComponent.prototype.ngOnInit = function () {
        var settings = this.noteService.getNoteSettings();
        this.colors = settings.colors;
    };
    return NoteColorComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], NoteColorComponent.prototype, "selectedColor", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], NoteColorComponent.prototype, "newValue", void 0);
NoteColorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'note-color',
        templateUrl: 'color.template.html',
        styles: ["\n          .like-label {\n            text-transform: uppercase;\n            font-size: 10px;\n            display:inline-block;\n            margin-bottom:5px;\n          }\n          .color-wrapper{\n            margin-bottom: 15px;\n          }\n          .color{\n            display: inline-block;\n            width: 40px;\n            height: 40px;\n            border-radius: 50%;\n            margin-right: 10px;\n            cursor: pointer;\n            box-shadow: 1px 1px 2px rgba(0,0,0,.3);\n          }\n          .active {\n            background: #444;\n            width: 40px;\n            height: 40px;\n            border:solid #fff 5px;\n            box-shadow: 0 0 2px 3px rgba(43, 162, 224, .6);\n          }\n  "]
    }),
    __metadata("design:paramtypes", [index_1.NoteService])
], NoteColorComponent);
exports.NoteColorComponent = NoteColorComponent;
//# sourceMappingURL=note_color.component.js.map