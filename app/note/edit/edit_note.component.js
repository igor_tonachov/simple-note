"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var note_service_1 = require("../../services/note.service");
var EditNoteComponent = (function () {
    function EditNoteComponent(fb, noteService, router, elRef, activatedRoute) {
        this.fb = fb;
        this.noteService = noteService;
        this.router = router;
        this.elRef = elRef;
        this.activatedRoute = activatedRoute;
        this.tags = [];
        this.categories = [];
        this.selectedColor = '';
        this.newNoteForm = fb.group({
            'title': '',
            'description': '',
            'color': '#f1c40f',
            'categories': ''
        });
    }
    EditNoteComponent.prototype.selectNoteColor = function (color) {
        this.selectedColor = color;
    };
    EditNoteComponent.prototype.updateNote = function (note) {
        var _this = this;
        note.id = this.note_id;
        note.color = this.selectedColor;
        note.tags = this.tags;
        this.noteService.update(note).subscribe(function (note) {
            _this.router.navigateByUrl('');
        });
    };
    EditNoteComponent.prototype.getNote = function (id) {
        var _this = this;
        this.noteService.getById(id).subscribe(function (data) {
            _this.newNoteForm.controls['title'].setValue(data.note.title);
            _this.newNoteForm.controls['description'].setValue(data.note.description);
            _this.newNoteForm.controls['categories'].setValue(data.note.categories);
            _this.selectedColor = data.note.color;
            _this.tags = data.note.tags;
        });
    };
    EditNoteComponent.prototype.setTags = function (e, el) {
        if (!el.value)
            return;
        var t = el.value.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
        var temp = t.split(',');
        this.tags = this.tags.concat(temp);
        el.value = null;
    };
    EditNoteComponent.prototype.deleteTag = function (tag) {
        var index = this.tags.indexOf(tag);
        this.tags.splice(index, 1);
    };
    EditNoteComponent.prototype.getNoteSettings = function () {
        var settings = this.noteService.getNoteSettings();
        this.categories = settings.categories;
    };
    EditNoteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getNoteSettings();
        this.activatedRoute.params.subscribe(function (params) {
            var id = params['id'];
            _this.note_id = id;
            _this.getNote(id);
        });
    };
    return EditNoteComponent;
}());
EditNoteComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'edit_note.template.html',
        styleUrls: ['../note_form.css']
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        note_service_1.NoteService,
        router_1.Router,
        core_1.ElementRef,
        router_1.ActivatedRoute])
], EditNoteComponent);
exports.EditNoteComponent = EditNoteComponent;
//# sourceMappingURL=edit_note.component.js.map