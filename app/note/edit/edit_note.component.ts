import { Component, Input , OnInit, ElementRef, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import  { Note } from '../../models/index';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { NoteService } from '../../services/note.service';


@Component({
  moduleId: module.id,
  templateUrl : 'edit_note.template.html',
  styleUrls: ['../note_form.css']
})

export class EditNoteComponent implements OnInit {
  tags:string[] =[];
  note:Note;
  note_id:string;
  categories:string[] = [];
  newNoteForm : FormGroup;
  selectedColor:string = '';
  constructor(
    private fb: FormBuilder,
    private noteService: NoteService,
    private router: Router,
    private elRef: ElementRef,
    private activatedRoute: ActivatedRoute){
    this.newNoteForm = fb.group({
      'title' : '',
      'description': '',
      'color' : '#f1c40f',
      'categories': ''
    })
  }
  selectNoteColor(color:string) {
    this.selectedColor = color;
  }
  updateNote(note:Note){
    note.id = this.note_id;
    note.color = this.selectedColor;
    note.tags = this.tags;
    this.noteService.update(note).subscribe(note => {
    this.router.navigateByUrl('');
    });

  }
  getNote(id:string){
    this.noteService.getById(id).subscribe(data => {
    this.newNoteForm.controls['title'].setValue(data.note.title);
    this.newNoteForm.controls['description'].setValue(data.note.description);
    this.newNoteForm.controls['categories'].setValue(data.note.categories);
    this.selectedColor = data.note.color;
    this.tags = data.note.tags;
     });
  }
  setTags(e:Event, el:HTMLInputElement) {
    if(!el.value) return;
    let t = el.value.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
    let temp = t.split(',');
    this.tags = this.tags.concat(temp);
    el.value = null;
  }
  deleteTag(tag:string) {
    let index = this.tags.indexOf(tag);
    this.tags.splice(index, 1);
  }
  getNoteSettings(){
    let settings = this.noteService.getNoteSettings();
    this.categories = settings.categories;
  }
  ngOnInit(){
    this.getNoteSettings();
    this.activatedRoute.params.subscribe((params: Params) => {
        let id:string = params['id'];
        this.note_id = id;
        this.getNote(id);
      });
  }
}
