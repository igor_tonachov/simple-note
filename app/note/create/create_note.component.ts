import { Component, Input, Renderer, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Router} from '@angular/router';

import { NoteService } from '../../services/index';
import  { Note } from '../../models/index';


@Component({
  moduleId: module.id,
  templateUrl : 'create_note.template.html',
  styleUrls: ['../note_form.css']
})
export class CreateNoteComponent implements OnInit {
  tags:string[] = [];
  categories:string[] = [];
  newNoteForm : FormGroup;
  selectedColor:string = '';
  constructor(private fb: FormBuilder,
              private noteService: NoteService,
              private router: Router,
              private renderer: Renderer,
              private elRef: ElementRef
              ){
    this.newNoteForm = fb.group({
      'title' : '',
      'description': '',
      'color' : '#f1c40f',
      'categories': ''
    })
  }
  selectNoteColor(color:string) {
    this.selectedColor = color;
  }

  setTags(e:Event, el:HTMLInputElement) {
    if(!el.value) return;
    let t = el.value.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
    let temp = t.split(',');
    this.tags = this.tags.concat(temp);
    el.value = null;
  }

  deleteTag(tag:string) {
    let index = this.tags.indexOf(tag);
    this.tags.splice(index, 1);
  }

  createNote(note:Note){
    note.color = this.selectedColor;
    note.tags = this.tags;
    this.noteService.create(note).subscribe(createdNote => {
      this.router.navigateByUrl('/');
    });
  }
  getNoteSettings(){
    let settings = this.noteService.getNoteSettings();
    this.categories = settings.categories;

  }

  ngOnInit(){
    this.getNoteSettings();
  }
}
