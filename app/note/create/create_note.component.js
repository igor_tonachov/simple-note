"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var index_1 = require("../../services/index");
var CreateNoteComponent = (function () {
    function CreateNoteComponent(fb, noteService, router, renderer, elRef) {
        this.fb = fb;
        this.noteService = noteService;
        this.router = router;
        this.renderer = renderer;
        this.elRef = elRef;
        this.tags = [];
        this.categories = [];
        this.selectedColor = '';
        this.newNoteForm = fb.group({
            'title': '',
            'description': '',
            'color': '#f1c40f',
            'categories': ''
        });
    }
    CreateNoteComponent.prototype.selectNoteColor = function (color) {
        this.selectedColor = color;
    };
    CreateNoteComponent.prototype.setTags = function (e, el) {
        if (!el.value)
            return;
        var t = el.value.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
        var temp = t.split(',');
        this.tags = this.tags.concat(temp);
        el.value = null;
    };
    CreateNoteComponent.prototype.deleteTag = function (tag) {
        var index = this.tags.indexOf(tag);
        this.tags.splice(index, 1);
    };
    CreateNoteComponent.prototype.createNote = function (note) {
        var _this = this;
        note.color = this.selectedColor;
        note.tags = this.tags;
        this.noteService.create(note).subscribe(function (createdNote) {
            _this.router.navigateByUrl('/');
        });
    };
    CreateNoteComponent.prototype.getNoteSettings = function () {
        var settings = this.noteService.getNoteSettings();
        this.categories = settings.categories;
    };
    CreateNoteComponent.prototype.ngOnInit = function () {
        this.getNoteSettings();
    };
    return CreateNoteComponent;
}());
CreateNoteComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'create_note.template.html',
        styleUrls: ['../note_form.css']
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        index_1.NoteService,
        router_1.Router,
        core_1.Renderer,
        core_1.ElementRef])
], CreateNoteComponent);
exports.CreateNoteComponent = CreateNoteComponent;
//# sourceMappingURL=create_note.component.js.map