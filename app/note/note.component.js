"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../services/index");
var NoteComponent = (function () {
    function NoteComponent(elRef, noteService) {
        this.elRef = elRef;
        this.noteService = noteService;
        this.const = {};
        this.notes = [];
    }
    NoteComponent.prototype.filterNotes = function (e, filterType, val) {
        // this.notes = JSON.parse(localStorage.getItem('notes'));
        this.notes = this.noteService.getMyNotes();
        if (!val) {
            return;
        }
        ;
        if (filterType === 'color') {
            if (e.className.indexOf('active') > 0) {
                this.selectedColor = '';
                return;
            }
            ;
            this.selectedColor = val;
        }
        if (filterType === 'tags') {
            this.notes = this.notes.filter(function (note) {
                return (note.tags.indexOf(val) > -1);
            });
            return;
        }
        if (val) {
            this.notes = this.notes.filter(function (note) {
                return note[filterType] === val;
            });
        }
    };
    NoteComponent.prototype.removeNote = function (id) {
        this.notes = this.notes.filter(function (item) {
            return item.id !== id;
        });
        this.noteService.delete(id).subscribe(function (data) {
        });
    };
    NoteComponent.prototype.getNoteSettings = function () {
        this.const = this.noteService.getNoteSettings();
    };
    NoteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getNoteSettings();
        this.notes = this.noteService.getMyNotes();
        if (this.notes.length === 0) {
            this.noteService.getNotes().subscribe(function (data) {
                _this.notes = data.notes;
            });
        }
    };
    return NoteComponent;
}());
NoteComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'note.template.html',
        styleUrls: ['note.styles.css']
    }),
    __metadata("design:paramtypes", [core_1.ElementRef, index_1.NoteService])
], NoteComponent);
exports.NoteComponent = NoteComponent;
//# sourceMappingURL=note.component.js.map