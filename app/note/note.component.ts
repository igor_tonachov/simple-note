import { Component, OnInit, ElementRef } from '@angular/core';
import { NoteService } from '../services/index';
import { Note } from '../models/index';
import {Observable} from 'rxjs/Observable';


@Component ({
  moduleId: module.id,
  templateUrl: 'note.template.html',
  styleUrls: ['note.styles.css']
})

export class NoteComponent implements OnInit{
  const:any = {};
  selectedColor:string;
  notes: Note[] = [];
  constructor(private elRef:ElementRef, private noteService: NoteService){
  }

  filterNotes(e:HTMLElement, filterType:string, val:string) {
    // this.notes = JSON.parse(localStorage.getItem('notes'));
    this.notes = this.noteService.getMyNotes();
    if(!val) {return};
    if(filterType === 'color') {
      if(e.className.indexOf('active') > 0) {
        this.selectedColor = ''
        return;
      };
      this.selectedColor = val;
    }
    if(filterType === 'tags') {
      this.notes = this.notes.filter(note =>{
        return (note.tags.indexOf(val) > -1);
      });
      return;
    }
    if(val){
      this.notes = this.notes.filter(note =>{
        return note[filterType] === val;
      })
    }
  }
  removeNote(id:string){
      this.notes = this.notes.filter(item => {
        return item.id !== id;
      })
      this.noteService.delete(id).subscribe(data => {
      });
  }
  getNoteSettings(){
    this.const = this.noteService.getNoteSettings();
  }

  ngOnInit(){
    this.getNoteSettings();
    this.notes = this.noteService.getMyNotes();
    if(this.notes.length === 0){
      this.noteService.getNotes().subscribe(data => {
        this.notes = data.notes;
      });
    }
  }
}
