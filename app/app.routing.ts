﻿import { Routes, RouterModule } from '@angular/router';

// import { NoteComponent } from './note/note.component';
import { CreateNoteComponent, EditNoteComponent, NoteComponent } from './note/index';

const appRoutes: Routes = [
    { path: '', component: NoteComponent},
    { path: 'new', component: CreateNoteComponent },
    { path: 'edit/:id', component: EditNoteComponent },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
